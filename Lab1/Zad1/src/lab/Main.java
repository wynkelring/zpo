package lab;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int liczba = 0b1101_1000;
        try(Scanner s = new Scanner(System.in)) {
            System.out.println("Przekonwertuj liczbe 0b1101_1000 na");
            System.out.println("dziesięć/trzy/szesnaście:");
            String conv = s.nextLine();

            switch(conv.toUpperCase()){
                case "DZIESIĘĆ":
                    System.out.println(Integer.toString(liczba, 10));
                    break;
                case "TRZY":
                    System.out.println(Integer.toString(liczba, 3));
                    break;
                case "SZESNAŚCIE":
                    System.out.println(Integer.toString(liczba, 16).toUpperCase());
                    break;
                default:
                    System.out.println("Wprowadziłeś złą komendę!");
            }
        }
    }
}

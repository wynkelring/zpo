package lab;

import lab.exceptions.IncorrectPeselChecksumException;
import lab.exceptions.PESELTooLongException;

import java.math.BigInteger;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
    	System.out.println("Wprowadź PESEL: ");
	    String pesel = null;
    	try (Scanner s = new Scanner(System.in)) {
    		pesel= s.nextLine();
	    }

	    Pesel p = new Pesel(pesel);

	    try {
		    p.isGood();
		    /*System.out.println("Dzień: " + p.getDay());
		    System.out.println("Miesiąc: " + p.getMonth());
		    System.out.println("Rok: " + p.getYear());
			System.out.println("Płeć: " + p.gender());*/
			System.out.println(p);
	    } catch (InputMismatchException | NumberFormatException e) {
			System.out.println("To nie jest liczba!");
		} catch (PESELTooLongException | IncorrectPeselChecksumException e) {
	    }
    }
}

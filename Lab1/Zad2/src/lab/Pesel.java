package lab;

import lab.exceptions.IncorrectPeselChecksumException;
import lab.exceptions.PESELTooLongException;

import java.math.BigInteger;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Pesel {
    String pesel;
    final int validateValues[] = {9,7,3,1,9,7,3,1,9,7};

    public Pesel(String pesel){
        this.pesel = pesel;
    }

    public void isGood() throws PESELTooLongException, IncorrectPeselChecksumException {
        int checksum = 0;
        if(pesel.length() != 11) {
            throw new PESELTooLongException();
        }
        for(int i = 0; i < pesel.length()-1; i++){
            checksum += Integer.parseInt(pesel.substring(i, i+1)) * validateValues[i];
        }
        if((checksum % 10) != Integer.parseInt(pesel.substring(10,11))){
            throw new IncorrectPeselChecksumException();
        }
    }

    public int getYear() {
        int stulecie = Integer.parseInt(pesel.substring(2, 3));
        int year;
        if ((8 == stulecie) || (stulecie == 9)) {
            year = 1800;
        } else if ((0 == stulecie) || (stulecie == 1)) {
            year = 1900;
        } else if ((2 == stulecie) || (stulecie == 3)) {
            year = 2000;
        } else if ((4 == stulecie) || (stulecie == 5)) {
            year = 2100;
        } else {
            year = 2200;
        }
        return Integer.parseInt(pesel.substring(0, 2)) + year;
    }
    public int getMonth() {
        return Integer.parseInt(pesel.substring(2, 4));
    }
    public int getDay() {
        return Integer.parseInt(pesel.substring(4, 6));
    }

    public String gender() {
        String pesels = String.valueOf(pesel).toString().substring(10, 11);
        int sex = Integer.parseInt(pesels);
        if ((sex % 2) == 0) {
            return "Kobieta";
        } else {
            return "Mężczyzna";
        }
    }

    public String toString(){
        return "Data urodzenia: " + this.getDay() + "." + this.getMonth() + "." + getYear() + "\nPłeć: " + gender();
    }
}

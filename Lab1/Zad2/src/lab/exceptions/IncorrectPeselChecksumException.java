package lab.exceptions;

public class IncorrectPeselChecksumException extends Exception {
    public IncorrectPeselChecksumException() {
        System.out.println("PESEL nieprawidłowy, suma weryfikacyjna niepoprawna.");
    }
}

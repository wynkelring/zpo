package lab;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String number;
        try (Scanner s = new Scanner(System.in)) {
            number = s.nextLine();
        }

        NumberTranslator num = new NumberTranslator(number, 3);

        try {
            num.isNumberGood();
            System.out.println(num.toString());
        } catch(WrongNumberException | InputMismatchException | NumberFormatException e){
        }

    }
}

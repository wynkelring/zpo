package lab;

import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Scanner;

public class NumberTranslator {
    public String number;
    private int numLength;
    private boolean minus = false;

    public NumberTranslator(String number, int length){
        this.number = number;
        this.numLength = length;
    }

    public void isNumberGood() throws WrongNumberException {
        if((number.length() > numLength) && (Integer.parseInt(number) > 0)){
            throw new WrongNumberException("Niepoprawna długość liczby");
        } else if(((number.length() > numLength + 1) || (number.length() <= numLength)) && (Integer.parseInt(number) <= 0)){
            throw new WrongNumberException("Niepoprawna długość liczby");
        } else if(String.valueOf(Integer.parseInt(number)).length() < numLength){
            throw new WrongNumberException("Niepoprawna długość liczby");
        } else {
        }
    }
    private String translateNumber(String num){
        HashMap<String, String> numbers = new HashMap<>();
        numbers.put("-", "minus");
        numbers.put("0", "zero");
        numbers.put("1", "jeden");
        numbers.put("2", "dwa");
        numbers.put("3", "trzy");
        numbers.put("4", "cztery");
        numbers.put("5", "pięć");
        numbers.put("6", "sześć");
        numbers.put("7", "siedem");
        numbers.put("8", "osiem");
        numbers.put("9", "dziewięć");

        return numbers.get(num);
    }
    public String toString(){
        //stringuilder
        String numberWords = this.number.concat(" = ");

        for(int i = 0; i < number.length(); i++){
            numberWords += (translateNumber(this.number.substring(i, i + 1)).concat(" "));
        }

        return numberWords;
    }
}

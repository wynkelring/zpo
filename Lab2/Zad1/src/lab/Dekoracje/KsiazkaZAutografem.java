package lab.Dekoracje;

import lab.BookRequirementsException;
import lab.KsiazkaDecorator;
import lab.Publikacja;

public class KsiazkaZAutografem extends KsiazkaDecorator {
    public String autograf;

    public KsiazkaZAutografem(Publikacja newKsiazka, String autograf) throws BookRequirementsException {
        super(newKsiazka);
        this.autograf = autograf;
        if(newKsiazka instanceof KsiazkaZAutografem) {
            if(((KsiazkaDecorator) newKsiazka).maAutograf) {
                throw new BookRequirementsException("Autograf może być tylko jeden");
            }
        }
        if(newKsiazka instanceof KsiazkaDecorator){
            dziedziczCechy();
            if(((KsiazkaDecorator) newKsiazka).maAutograf == true){
                throw new BookRequirementsException("Autograf może być tylko jeden");
            }
        }
        this.maAutograf = true;
    }

    @Override public String toString(){
        return tempKsiazka.toString() + this.autograf + " |";}

}

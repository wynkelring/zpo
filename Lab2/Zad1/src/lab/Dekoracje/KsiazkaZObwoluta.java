package lab.Dekoracje;

import lab.BookRequirementsException;
import lab.Ksiazka;
import lab.KsiazkaDecorator;
import lab.Publikacja;

public class KsiazkaZObwoluta extends KsiazkaDecorator {

    public KsiazkaZObwoluta(Publikacja newKsiazka) throws BookRequirementsException {
        super(newKsiazka);
        if(newKsiazka instanceof Ksiazka) {
            throw new BookRequirementsException("Nie można obłożyć obwolutą książki, która nie posiada okładki");
        }
        if(newKsiazka instanceof KsiazkaZObwoluta) {
            if(((KsiazkaDecorator) newKsiazka).maObwolute) {
                throw new BookRequirementsException("Obwoluta może być jedna");
            }
        }
        if(newKsiazka instanceof KsiazkaDecorator){
            dziedziczCechy();
            if(((KsiazkaDecorator) newKsiazka).maObwolute == true){
                throw new BookRequirementsException("Obwoluta może być jedna");
            }
        }
        this.maObwolute = true;
    }

    @Override public String toString(){
        return tempKsiazka.toString() + "Oblozona Obwolutem | ";
    }

}

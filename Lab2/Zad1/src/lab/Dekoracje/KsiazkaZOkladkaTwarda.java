package lab.Dekoracje;

import lab.BookRequirementsException;
import lab.KsiazkaDecorator;
import lab.Publikacja;

public class KsiazkaZOkladkaTwarda extends KsiazkaDecorator implements KsiazkaZOkladka {
    public KsiazkaZOkladkaTwarda(Publikacja newKsiazka) throws BookRequirementsException {
        super(newKsiazka);
        if(newKsiazka instanceof KsiazkaZOkladka) {
            if(((KsiazkaDecorator) newKsiazka).maOkladke) {
                throw new BookRequirementsException("Może być tylko jedna okładka");
            }
        }
        if(newKsiazka instanceof KsiazkaDecorator){
            dziedziczCechy();
            if(((KsiazkaDecorator) newKsiazka).maOkladke == true){
                throw new BookRequirementsException("Może być tylko jedna okładka");
            }
        }
        this.maOkladke = true;
    }

    @Override public String toString(){
        return tempKsiazka.toString() + "Okładka twarda | ";
    }

}

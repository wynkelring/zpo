package lab.Gatunki;

import lab.Ksiazka;

public class Poemat extends Ksiazka {

    public Poemat(String autor, String tytul, int strony) {
        super(autor, tytul, strony);
    }
    @Override public String toString(){
        return "| " + this.autor + " | " + this.tytul + " | " + this.strony + " | Poemat | ";
    }
}

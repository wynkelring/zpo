package lab.Gatunki;

import lab.Ksiazka;
import lab.Wydawnictwo;

public class PowiescHistoryczna extends Ksiazka {

    public PowiescHistoryczna(String autor, String tytul, int strony) {
        super(autor, tytul, strony);
    }
    @Override public String toString(){
        return "| " + this.autor + " | " + this.tytul + " | " + this.strony + " | Powieść Historyczna | ";
    }
}

package lab;

public class Ksiazka implements Publikacja {
    public String autor;
    public String tytul;
    public int strony;

    public Ksiazka(String autor, String tytul, int strony){
        this.autor = autor;
        this.tytul = tytul;
        this.strony = strony;
    }

    @Override public String getAutor() {
        return this.autor;
    }

    @Override public String getTytul() {
        return this.tytul;
    }

    @Override public int getStrony() {
        return this.strony;
    }

    @Override public String toString(){
        return "| " + this.autor + " | " + this.tytul + " | " + this.strony + " | ";
    }
}

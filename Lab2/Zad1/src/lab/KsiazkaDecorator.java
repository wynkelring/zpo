package lab;

public abstract class KsiazkaDecorator implements Publikacja {
    protected Publikacja tempKsiazka;

    public boolean maObwolute;
    public boolean maOkladke;
    public boolean maAutograf;

    public KsiazkaDecorator(Publikacja newKsiazka){
        this.tempKsiazka = newKsiazka;
        this.maObwolute = false;
        this.maOkladke = false;
        this.maAutograf = false;
    }

    @Override public String getAutor() {
        return tempKsiazka.autor;
    }

    @Override public String getTytul() {
        return tempKsiazka.tytul;
    }

    @Override public int getStrony() {
        return tempKsiazka.strony;
    }

    public String toString() { return tempKsiazka.toString(); }

    public void dziedziczCechy(){
        this.maOkladke = ((KsiazkaDecorator) tempKsiazka).maOkladke;
        this.maObwolute = ((KsiazkaDecorator) tempKsiazka).maObwolute;
        this.maAutograf = ((KsiazkaDecorator) tempKsiazka).maAutograf;
    }
}

package lab;

import lab.Dekoracje.KsiazkaZAutografem;
import lab.Dekoracje.KsiazkaZObwoluta;
import lab.Dekoracje.KsiazkaZOkladkaTwarda;
import lab.Dekoracje.KsiazkaZOkladkaZwykla;
import lab.Wydawnictwa.WydawnictwoPoematow;
import lab.Wydawnictwa.WydawnictwoPowiesciHistorycznych;
import lab.Wydawnictwa.WydawnictwoThrillerow;

public class Main {

    public static void main(String[] args) throws BookRequirementsException {

        Publikacja k1 = new Ksiazka("Adam Mickiewicz", "Pan Tadeusz", 340);
        System.out.println(k1);

        Publikacja k2 = new Ksiazka("Adam Mickiewicz", "Dziady", 130);
        System.out.println(k2);

        Publikacja kk1 = new KsiazkaZOkladkaZwykla(k1);
        System.out.println(kk1);

        Publikacja kk2 = new KsiazkaZOkladkaTwarda(k2);
        System.out.println(kk2);

        Publikacja kk3 = new KsiazkaZAutografem(k2, "Drogiej Hani - Adam Mickiewicz");
        System.out.println(kk3);

        try {
            Publikacja fakeBook = new KsiazkaZObwoluta(k1);  // wyjątek! Nie można obłożyć obwolutą książki, która nie posiada okładki
            System.out.println(fakeBook);
        } catch (BookRequirementsException e){}

        Publikacja kkk2 = new KsiazkaZObwoluta(kk2); // a tu OK
        System.out.println(kkk2);

        try {
            Publikacja kkk3 = new KsiazkaZOkladkaTwarda(kk2);
            System.out.println(kkk3);
        } catch (BookRequirementsException e){}

        try {
            Publikacja kkk4 = new KsiazkaZOkladkaZwykla(kk2);
            System.out.println(kkk4);
        } catch (BookRequirementsException e){}

        try {
            Publikacja odrzut = new KsiazkaZObwoluta(kkk2);  // wyjątek! Obwoluta może być jedna
            System.out.println(odrzut);
        } catch (BookRequirementsException e){}

        Publikacja dziadyZAutografemWieszcza = new KsiazkaZAutografem(kk2, "Drogiej Hani - Adam Mickiewicz");
        System.out.println(dziadyZAutografemWieszcza);

        try {
            Publikacja dziadyZDwomaAutografami = new KsiazkaZAutografem(dziadyZAutografemWieszcza, "Haniu, to nieprawda, Dziady napisałem ja, Julek Słowacki!"); // wyjątek! Autograf może być tylko jeden
            System.out.println(dziadyZDwomaAutografami);
        } catch (BookRequirementsException e){}


        try {
            Publikacja dwieokladki = new KsiazkaZOkladkaZwykla(kkk2); // wyjątek! Autograf może być tylko jeden
            System.out.println(dwieokladki);
        } catch (BookRequirementsException e){}

        try {
            Publikacja autog = new KsiazkaZAutografem(kkk2, "autograf");
            Publikacja autogg = new KsiazkaZObwoluta(autog);
            System.out.println(autogg);
        } catch (BookRequirementsException e){}


        System.out.println("\n\n\n");
        Wydawnictwo w = Wydawnictwo.getInstance("Józef Ignacy Kraszewski");
        Wydawnictwo w1 = Wydawnictwo.getInstance("Henryk Sienkiewicz");
        Wydawnictwo w2 = Wydawnictwo.getInstance("Paula Hawkins");
        Ksiazka p = w.createBook("Masław", 280);
        Ksiazka p1 = w1.createBook("po", 500);
        Ksiazka p2 = w2.createBook("th", 180);
        System.out.println(p);
        System.out.println(p1);
        System.out.println(p2);
        try {
            Wydawnictwo w3 = Wydawnictwo.getInstance("Nieznany");
            Ksiazka p3 = w3.createBook("Masław 2", 370);
            System.out.println(p3);
        } catch (BookRequirementsException e) {}
        /* W zależności od autora wybieramy odpowiednie wydawnictwo. Wpisać kilka wariantów. Tu powstanie wydawnictwo powieści historycznych */

        /* Tworzy książkę klasy PowiescHistoryczna z podanym tytułem i liczbą stron. Autor przekazany będzie z wydawnictwa */



    }
}

package lab;

public interface Publikacja {
    String autor = null;
    String tytul = null;
    int strony = 0;

    String getAutor();
    String getTytul();
    int getStrony();
    String toString();
}

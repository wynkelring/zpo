package lab.Wydawnictwa;

import lab.Gatunki.Poemat;
import lab.Gatunki.PowiescHistoryczna;
import lab.Ksiazka;
import lab.Wydawnictwo;

public class WydawnictwoPoematow extends Wydawnictwo {
    static private Wydawnictwo instance;
    String autor;

    public WydawnictwoPoematow(String autor){
        super(autor);
        this.autor = autor;
    }

    static public Wydawnictwo getInstance(String autor){
        if (instance == null) {
            instance = new WydawnictwoPoematow(autor);
        }
        return instance;

    }

    public Ksiazka createBook(String tytul, int strony) {
        return new Poemat(autor, tytul, strony);
    }
}

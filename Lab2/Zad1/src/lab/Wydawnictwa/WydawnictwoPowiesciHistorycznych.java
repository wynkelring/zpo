package lab.Wydawnictwa;

import lab.Gatunki.PowiescHistoryczna;
import lab.Ksiazka;
import lab.Wydawnictwo;

public class WydawnictwoPowiesciHistorycznych extends Wydawnictwo {
    static private Wydawnictwo instance;
    String autor;

    public WydawnictwoPowiesciHistorycznych(String autor){
        super(autor);
        this.autor = autor;
    }

    static public Wydawnictwo getInstance(String autor){
        if (instance == null) {
            instance = new WydawnictwoPowiesciHistorycznych(autor);
        }
        return instance;

    }



    public Ksiazka createBook(String tytul, int strony) {
        return new PowiescHistoryczna(autor, tytul, strony);
    }
}

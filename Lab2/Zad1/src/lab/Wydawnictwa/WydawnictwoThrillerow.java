package lab.Wydawnictwa;

import lab.Gatunki.PowiescHistoryczna;
import lab.Gatunki.Thriller;
import lab.Ksiazka;
import lab.Wydawnictwo;

public class WydawnictwoThrillerow extends Wydawnictwo {
    static private Wydawnictwo instance;
    String autor;

    public WydawnictwoThrillerow(String autor){
        super(autor);
        this.autor = autor;
    }

    static public Wydawnictwo getInstance(String autor){
        if (instance == null) {
            instance = new WydawnictwoThrillerow(autor);
        }
        return instance;

    }
    public Ksiazka createBook(String tytul, int strony) {
        return new Thriller(autor, tytul, strony);
    }
}

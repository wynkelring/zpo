package lab;

import lab.Wydawnictwa.WydawnictwoPoematow;
import lab.Wydawnictwa.WydawnictwoPowiesciHistorycznych;
import lab.Wydawnictwa.WydawnictwoThrillerow;

public abstract class Wydawnictwo {
    String autor;

    public Wydawnictwo(String autor){
        this.autor = autor;
    }


    public static Wydawnictwo getInstance(String autor) throws BookRequirementsException {
        if(autor.equals("Józef Ignacy Kraszewski")){
            return WydawnictwoPoematow.getInstance(autor);
        } else if (autor.equals("Henryk Sienkiewicz")){
            return WydawnictwoPowiesciHistorycznych.getInstance(autor);
        } else if (autor.equals("Paula Hawkins")) {
            return WydawnictwoThrillerow.getInstance(autor);
        } else {
            throw new BookRequirementsException("Trudno przydzielic tego autora: ".concat(autor));
        }
    }

    public Ksiazka createBook(String tytul, int strony) {
        return null;
    }
}

package lab;

import java.time.DateTimeException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class Main {

    public static void main(String[] args) {
        // a) ile dni trwała II wojna światowa (wliczając zarówno 1.09.1939, jak i 8.05.1945);
        LocalDateTime a1 = LocalDateTime.of(1939,9,1,0,0);
        LocalDateTime a2 = LocalDateTime.of(1945, 5, 8,0,0);
        System.out.println("Różnica pomiędzy " + a1 + " i " + a2 + " w dniach:");
        Duration period = Duration.between(a1, a2);
        long days = ChronoUnit.DAYS.between(a1, a2);
        System.out.println(days);

        // b) którego dnia i miesiąca wypada 68-my dzień roku 2016;
        int year = 2016, day = 68;
        LocalDateTime b = LocalDateTime.of(year,1,1,0,0);
        System.out.println("\n" + day + "-my dzień roku 2016:");
        System.out.println("Miesiąc: " + b.plusDays(day-1).getMonth());
        System.out.println("Dzień: " + b.plusDays(day-1).getDayOfMonth());

        // c) ile w swoim życiu przeżyłeś/aś dni 29 lutego. Nie zakładaj z góry, że np. bieżący rok nie
        //jest przestępny.
        int bYear = 1996, bMonth = 7, bDay = 12;
        try {
            /*LocalDateTime c1 = LocalDateTime.of(bYear, bMonth, bDay, 0, 0);
            LocalDateTime c2 = LocalDateTime.of(bYear,2,29,0,0);
            long ye = ChronoUnit.YEARS.between(c1, LocalDateTime.now());
            int leap = 0;
            long i = 1;
            if(bMonth <= 2 && bDay <= 29){
                i = 0;
            }
            for(; i <= ye; i++){
                if(c2.plusYears(i).getDayOfYear() == 366){
                    //System.out.println(c2.plusYears(i).getYear());
                    leap++;
                }
            }*/

            LocalDateTime c1 = LocalDateTime.of(bYear, bMonth, bDay, 0, 0);
            long ye = ChronoUnit.YEARS.between(c1, LocalDateTime.now());
            int leap = 0;
            while(c1.isBefore(LocalDateTime.now())){
                c1 = c1.plusDays(1);
                if(c1.toString().contains("-02-29")) {
                    leap++;
                }
            }
            System.out.println("\nIle w swoim życiu przeżyłem dni 29 lutego? " + leap);
        } catch (DateTimeException e) {
            System.out.println("Podałeś złą datę!");
        }

    }
}

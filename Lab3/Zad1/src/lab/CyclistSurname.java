package lab;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashSet;

public class CyclistSurname {
    HashSet<String> surname = new HashSet<>();

    public CyclistSurname() throws IOException {
        URL surnameUrl = new URL("http://szgrabowski.kis.p.lodz.pl/zpo18/nazwiska.txt");
        BufferedReader readSurnames = new BufferedReader(new InputStreamReader(surnameUrl.openStream()));

        String surnameFromFile;
        while ((surnameFromFile = readSurnames.readLine()) != null) {
            this.surname.add(surnameFromFile);
        }
        readSurnames.close();
    }
}

package lab;

import java.io.*;
import java.util.Comparator;
import java.util.logging.*;

public class Main {

    public static void main(String[] args) throws InterruptedException, IOException {
        Logger logger = null;
        try {
            Handler fh = new FileHandler("./wyscig.log", false);
            logger = Logger.getLogger(Main.class.getName());
            logger.addHandler(fh);
            logger.setLevel(Level.FINE);
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        }

        int cyclistOnRace = 15;

        CyclistSurname surnameList = new CyclistSurname();
        CyclistList firstRaceCyclists = new CyclistList(surnameList.surname, cyclistOnRace);

        System.out.println(firstRaceCyclists.cyclists);

        System.out.println("Wyścig się rozpoczyna!");
        logger.fine("Wyścig się rozpoczyna!");

        Race firstRace = new Race(firstRaceCyclists, cyclistOnRace);

        System.out.println("Koniec wyścigu!");
        System.out.println("Dziekujemy za oglądanie.\n");
        logger.fine("Koniec wyścigu! Dziekujemy za oglądanie.");

        firstRace.showRaceResults(false);
        System.out.println(firstRace.raceResults);
    }
}

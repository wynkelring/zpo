package lab;

public class Answer {
    private String answer;

    public String getAnswer() {
        return answer;
    }

    @Override
    public String toString() {
        return answer;
    }
}

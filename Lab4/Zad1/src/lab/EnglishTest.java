package lab;

import com.google.common.base.Stopwatch;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.text.similarity.LevenshteinDistance;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class EnglishTest {
    double score = 0.0;
    List<UserAnswer> userAnswers = new ArrayList<>();

    public EnglishTest(String path, int numberOfQuestions) throws IOException {
        Stopwatch stopwatch = Stopwatch.createStarted();
            QuestionList question = new QuestionList(path);
            for(int i = 0; i < numberOfQuestions; i++) {
                AskQuestion(question.getQuestion());
            }
        stopwatch.stop();

        DecimalFormat df = new DecimalFormat("#.##");
        double sec = stopwatch.elapsed(TimeUnit.MILLISECONDS) / 1000.00;
        System.out.println("Twój wynik: " + score);
        System.out.println("Rozwiązałeś test w " + df.format(sec) + "s");
        System.out.println("Podaj swoje imię i nazwisko");
        String filename = new Scanner(System.in).nextLine();
        filename = filename.replaceAll("\\s+","_");
        try (Writer writer = new FileWriter(filename + ".json")) {
            Gson gson = new GsonBuilder().create();
            String json = gson.toJson(userAnswers);
            writer.write(json);
        }  catch (IOException e) {
            System.out.println(e);
        }
    }

    private void AskQuestion(Question question){
        System.out.println("Podaj tłumaczenie słowa: " + question.getQuestion());
        String answer = new Scanner(System.in).nextLine();
        UserAnswer ua = new UserAnswer(question.getQuestion(), answer);
        userAnswers.add(ua);
        ValidateAnswer(answer, question.answers);
    }

    private void ValidateAnswer(String given, List correct) {
        int levenshteinDistance = 2;
        for(int i = 0; i < correct.size(); i++){
            levenshteinDistance = LevenshteinDistance.getDefaultInstance().apply(given.toLowerCase(), correct.get(i).toString());
            if (levenshteinDistance == 1) {
                score += 0.5;
            } else if (levenshteinDistance == 0) {
                score += 1.0;
            }
        }
    }
}

package lab;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        String path = "resources/PolEngTest.json";
        int numberOfQuestions = 5;

        EnglishTest test = new EnglishTest(path, numberOfQuestions);
    }
}

package lab;

import java.util.List;

public class Question {

    String question;
    List<Answer> answers;


    public String getQuestion() {
        return question;
    }

    @Override
    public String toString() {
        return question + " = " + answers;
    }
}

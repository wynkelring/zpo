package lab;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Random;

public class QuestionList {
    ArrayList<Question> questionList;

    public QuestionList(String path) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(path))) {
            Type listType = new TypeToken<ArrayList<Question>>() {}.getType();
            questionList = new Gson().fromJson(reader, listType);
        } catch (FileNotFoundException e){
            System.out.println(e);
        } catch (IOException e){
            System.out.println(e);
        }
    }

    public Question getQuestion(){
        Question question = this.questionList.get(new Random().nextInt(this.questionList.size()));
        this.questionList.remove(question);
        return question;
    }

}

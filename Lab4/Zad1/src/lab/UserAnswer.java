package lab;

public class UserAnswer {
    String question;
    String answer;

    public UserAnswer(String question, String answer){
        this.question = question;
        this.answer = answer;
    }

    @Override
    public String toString() {
        return question + " = " + answer;
    }
}

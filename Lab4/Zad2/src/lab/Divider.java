package lab;

import com.google.common.base.Splitter;

import java.util.ArrayList;
import java.util.List;

public class Divider {
    private List<String> text = new ArrayList<String>();

    public Divider() {
    }

    public List<String>  divideString(String s, int length){
        if((length < 0) || (s == null)){
            throw new IllegalArgumentException();
        } else {
            Iterable<String> result = Splitter.fixedLength(length).trimResults().split(s);

            for (String temp : result) {
                text.add(temp);
            }
        }
        return text;
    }
}

package lab;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class DividerTests {

    String s = "Lorem Ipsum ble ble";
    Divider sd = new Divider();

    @DisplayName("Correct dividing - Length 2")
    @Test
    public void correctDividingLengthTwo() {
        int length = 2;
        List<String> array = new ArrayList<String>();
        array = Arrays.asList("Lo", "re", "m", "Ip", "su", "m", "bl", "e", "bl", "e");
        assertEquals(sd.divideString(s, length), array);
    }

    @DisplayName("Correct dividing - Length 3")
    @Test
    public void correctDividingLengthThree() {
        int length = 3;
        List<String> array = new ArrayList<String>();
        array = Arrays.asList("Lor", "em", "Ips", "um", "ble", "bl", "e");
        assertEquals(sd.divideString(s, length), array);
    }

    @DisplayName("Expection - length less than 0")
    @Test
    public void expectionLengthLessThanZero() {
        int length = -2;
        assertThrows(IllegalArgumentException.class, () -> {
            sd.divideString(s, length);
        });
    }

    @DisplayName("Expection - string is null")
    @Test
    public void expectionStringIsNull() {
        int length = 4;
        s = null;
        assertThrows(IllegalArgumentException.class, () -> {
            sd.divideString(s, length);
        });
    }
}

package lab;

import java.time.LocalDate;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        List<Student> studentCollection = new ArrayList<Student>();
        boolean compareJDK = true;

        Student s1 = new Student("Jan", "Kowalski", LocalDate.of(1996,1,13), 173, compareJDK);
        Student s2 = new Student("Adam", "Kowalski", LocalDate.of(1994,12,1), 175, compareJDK);
        Student s3 = new Student("Adam", "Wawrzyniak", LocalDate.of(1991,3,18), 171, compareJDK);
        Student s4 = new Student("Krzysztof", "Rowak", LocalDate.of(1992,12,9), 178, compareJDK);
        Student s5 = new Student("Marcin", "Zając", LocalDate.of(1991,11,21), 186, compareJDK);
        Student s6 = new Student("Tomasz", "Robak", LocalDate.of(1992,7,31), 175, compareJDK);

        studentCollection.add(s1);
        studentCollection.add(s2);
        studentCollection.add(s3);
        studentCollection.add(s4);
        studentCollection.add(s5);
        studentCollection.add(s6);

        Set<Student> sortedStudentCollection = new TreeSet(studentCollection);


        for (Iterator<Student> it = sortedStudentCollection.iterator(); it.hasNext(); ) {
            System.out.println(it.next());
        }
    }
}

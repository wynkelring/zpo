package lab;

import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Ordering;

import java.time.LocalDate;
import java.util.Comparator;

public class Student implements Comparable<Student> {
    String name;
    String surname;
    LocalDate birthDate;
    int height;
    public boolean compareJDK = false;

    public Student(String name, String surname, LocalDate birthDate, int height, boolean compareJDK){
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.height = height;
        this.compareJDK = compareJDK;
    }
    private int getBirthdayYear(){
        return this.birthDate.getYear();
    }

    private String getSurname(){
        return this.surname;
    }

    private int getHeight(){
        return this.height;
    }

    public int compareTo(Student student){
        if(compareJDK) {
            return Comparator.comparingInt(Student::getBirthdayYear)
                    .thenComparing(s->{
                        return s.getSurname().substring(0,1).compareTo(student.getSurname().substring(0,1));
                    })
                    .thenComparingInt(Student::getHeight).reversed()
                    .compare(this, student);
        } else {
            return ComparisonChain.start()
                    .compare(this.getBirthdayYear(), student.getBirthdayYear(), Ordering.natural().reverse())
                    .compare(this.getSurname().substring(0, 1), student.getSurname().substring(0, 1))
                    .compare(this.getHeight(), student.getHeight(), Ordering.natural().reverse())
                    .result();
        }
    }

    public String toString(){
        return "Imię: " + this.name + " Nazwisko: " + this.surname + " Data urodzenia: " + this.birthDate + " Wzrost: " + this.height;
    }
}

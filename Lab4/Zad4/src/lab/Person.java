package lab;

import java.util.Comparator;

public class Person implements Comparable<Person> {
    String name;
    String surname;
    String country;
    int salary;

    public Person(String name, String surname, String country, int salary){
        this.name = name;
        this.surname = surname;
        this.country = country;
        this.salary = salary;
    }

    public int getSalary(){
        return this.salary;
    }

    public String getCountry() {
        return this.country;
    }
    public String getSurname() {
        return this.surname;
    }

    public int compareTo(Person person){
        return Comparator.comparingInt(Person::getSalary)
                .compare(this, person);
    }

    public String toString(){
        return this.name + " " + this.surname + " " + this.country + " " + this.salary;
    }
}
